use std::collections::HashMap;
use std::env;
use std::f32::consts;

use svg::{Document, Node};
use svg::node::element::{Path, Rectangle, Text};
use svg::node::element::path::{Command, Data, Parameters, Position};

fn main() {
    //setting image size
    let imgsize = 2048;
    
    //reading wind rose statblock
    let mut statblock = HashMap::new();
    let mut max_value = 0 as f32;
    for arg in env::args() {
        let arg_vec: Vec<&str> = arg.split('=').collect();
        if arg_vec.len() != 1 {
            statblock.insert(
                arg_vec[0].to_string(),
                arg_vec[1].parse::<f32>().unwrap()
            );

            if arg_vec[1].parse::<f32>().unwrap() > max_value {
                max_value = arg_vec[1].parse::<f32>().unwrap();
            }
        }
    }
    
    //converting statblock into cartesian points
    let mut points: Vec<(f32, f32)> = Vec::new();
    let mut additional_points: Vec<Vec<(f32, f32)>> = Vec::new();
    let mut label_points = HashMap::new();
    let mut statnum = 0;
    let scale = imgsize as f32 / max_value * 0.45;
    let size = statblock.len() as f32 / 2.0;
    for (label, value) in statblock {
        let mut radius;
        if value >= max_value {
            radius = value * scale - 10.0;
        } else {
            radius = value * scale + 10.0;
        }
        let theta = statnum as f32 / size * consts::PI - consts::PI / 2.0;

        points.push((
            radius * theta.cos() + imgsize as f32 / 2.0,
            radius * theta.sin() + imgsize as f32 / 2.0
        ));
        
        additional_points.push(Vec::new());
        for i in 0..=max_value as i32 {
            radius = i as f32 * scale;
            additional_points[statnum].push((
                radius * theta.cos() + imgsize as f32 / 2.0,
                radius * theta.sin() + imgsize as f32 / 2.0
            ));

            if i == max_value as i32 {
                label_points.insert(
                label.clone(), (
                    radius * theta.cos() + imgsize as f32 / 2.0,
                    radius * theta.sin() + imgsize as f32 / 2.0
                ));
            }
        }

        statnum += 1;
    }
    
    //initialising SVG document
    let background = Rectangle::new()
        .set("fill", "white")
        .set("x", 0)
        .set("y", 0)
        .set("width", imgsize)
        .set("height", imgsize);
    
    let mut document = Document::new()
        .set("viewBox", (0, 0, imgsize, imgsize))
        .add(background);
    
    
    //axes
    for axis in additional_points {
        let mut data = Data::new().move_to(axis[0]);
        for (x, y) in axis {
            let position = Parameters::from((x, y));
            data.append(Command::Line(Position::Absolute, position));
        }
        let axis_data = data.close().close();

        let axis_path = Path::new()
            .set("fill", "none")
            .set("stroke", "pink")
            .set("stroke-width", 6)
            .set("d", axis_data);

        document = document.clone().add(axis_path);
    }

    //wind rose
    let mut data = Data::new().move_to(points[0]);
    for (x, y) in points {
        let position = Parameters::from((x, y));
        data.append(Command::Line(Position::Absolute, position));
    }
    let wr_data = data.clone().close();
    
    let wr_path = Path::new()
        .set("fill", "none")
        .set("stroke", "red")
        .set("stroke-width", 9)
        .set("d", wr_data);

    document = document.clone().add(wr_path);

    //labels
    for (label, (x, y)) in label_points {
        let mut text = Text::new()
            .set("x", x)
            .set("y", y)
            .set("fill", "black")
            .set("stroke-width", 6)
            .set("stroke", "white")
            .set("paint-order", "stroke")
            .set("font-family", "monospace")
            .set("font-size", 32);

        if x > imgsize as f32 * 0.75 {
            text = text.clone().set("text-anchor", "end");
        } else if x < imgsize as f32 * 0.25 {
            text = text.clone().set("text-anchor", "start");
        } else {
            text = text.clone().set("text-anchor", "middle");
        }

        text.append(svg::node::Text::new(label));

        document = document.clone().add(text);
    }

    //rendering everything
    svg::save("image.svg", &document).unwrap();
}
